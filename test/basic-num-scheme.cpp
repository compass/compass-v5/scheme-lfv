#include <catch2/catch_test_macros.hpp>
#include <cstdlib>
#include <ctime>
#include <icmesh/Geomtraits.h>
#include <scheme_lfv/tpfa.h>
#include <scheme_lfv/vag.h>

static auto build_3Dmesh(icus::index_type N) {
  // Constant_axis_offset{origin, Nb_elements, dx}
  auto step = icmesh::Constant_axis_offset{0., N, 1. / N};
  return icmesh::mesh(icmesh::lattice(step, step, step));
}

template <std::size_t elem_i>
static auto get_close_to(auto &mesh, auto dir, auto x, double eps = 1.e-6) {
  auto centers = mesh.template get_isobarycenters<elem_i>();
  std::vector<icus::index_type> top_indices;
  for (auto &&[i, v] : centers.enumerate())
    if (fabs(v[dir] - x) < eps)
      top_indices.push_back(i);

  return mesh.elements[elem_i].extract(top_indices);
}

static void tpfa_check_sites_qt(auto &num_scheme, auto dirichlet, auto vol_qt,
                                auto sites_qt, double eps = 1.e-6) {
  auto cell_sites_qt =
      sites_qt.extract(num_scheme.sites.from_location(num_scheme.cells));
  auto face_sites_qt =
      sites_qt.extract(num_scheme.sites.from_location(dirichlet));
  for (auto &&[v, s] : std::views::zip(vol_qt, cell_sites_qt))
    CHECK(fabs(v - s) < eps);
  for (auto &&s : face_sites_qt)
    CHECK(fabs(s) < eps);
}

static void check_tpfa_trans(auto si, auto Dt, auto Ft, auto ref_Dt,
                             auto ref_Ft, double eps = 1.e-6) {
  CHECK(fabs(fabs(Dt(si)) - ref_Dt) < eps);
  CHECK(fabs(fabs(Dt(si + 1)) - ref_Dt) < eps);
  CHECK(fabs(Dt(si + 1) + Dt(si)) < eps);
  CHECK(fabs(fabs(Ft(si)) - ref_Ft) < eps);
  CHECK(fabs(fabs(Ft(si + 1)) - ref_Ft) < eps);
  CHECK(fabs(Ft(si + 1) + Ft(si)) < eps);
}

static auto is_vol_nodes(auto nodes, auto dirichlet) {
  auto is_volume_nodes = icus::make_field<bool>(nodes);
  is_volume_nodes.fill(true);
  auto dir_nodes_rebased = dirichlet.rebase(nodes);
  for (auto &&i : dir_nodes_rebased.mapping())
    is_volume_nodes(i) = false;
  return is_volume_nodes;
}

// vol_qt is randomly generated
static void vag_check_sites_qt(auto &num_scheme, auto dirichlet, auto vol_qt,
                               auto sites_qt, double eps = 1.e-14) {
  // check that dirichlet values are 0
  auto dir_qt = sites_qt.extract(num_scheme.sites.from_location(dirichlet));
  for (auto &&d : dir_qt)
    CHECK(fabs(d) < eps);
  // check that sum over the mesh quantity and the sites quantity are equals
  auto mesh_sum = 0.0;
  for (auto &&q : vol_qt)
    mesh_sum += q;
  auto sites_sum = 0.0;
  for (auto &&q : sites_qt)
    sites_sum += q;
  CHECK(fabs((mesh_sum - sites_sum) / mesh_sum) < eps);
}

static auto tpfa_scheme(auto &mesh, auto scheme_cells, auto permeability,
                        auto th_conductivity) {
  auto eps = 1.e-6;
  auto [Nx, Ny, Nz] = *mesh.shape;
  auto [origin, last_pts] = *mesh.bounding_box;
  auto Lx = last_pts[0] - origin[0];
  auto Ly = last_pts[1] - origin[1];
  auto Lz = last_pts[2] - origin[2];
  // Dirichlet : top faces (TPFA)
  auto dirichlet = get_close_to<mesh.face>(mesh, 2, last_pts[2]);
  // init the numerical scheme
  auto num_scheme = numscheme::TPFA(mesh, scheme_cells, dirichlet);
  CHECK(num_scheme.sites.size() == dirichlet.size() + scheme_cells.size());

  auto vol_qt = icus::Field(scheme_cells);
  std::srand(std::time(0)); // use current time as seed for random generator
  for (auto &&v : vol_qt)
    v = std::rand();
  auto sites_qt = num_scheme.distribute_volumes(vol_qt);
  tpfa_check_sites_qt(num_scheme, dirichlet, vol_qt, sites_qt);

  // init Darcy and Fourier Transmissivities
  auto Darcy_trans = num_scheme.build_transmissivities(permeability);
  auto Fourier_trans = num_scheme.build_transmissivities(th_conductivity);

  // test tips, flux stencil, trans
  CHECK(num_scheme.flux_tips.source().size() ==
        3 * Nx * Ny * Nz - Ny * Nz - Nx * Nz - Nx * Ny + dirichlet.size());

  auto sites_K = num_scheme.sites.from_location(scheme_cells);
  auto sites_Ki = sites_K.mapping();
  CHECK(sites_K.size() == scheme_cells.size());
  auto sites_F = num_scheme.sites.from_location(dirichlet);
  auto sites_Fi = sites_F.mapping();
  CHECK(sites_F.size() == dirichlet.size());
  auto nb_flux_by_sites = icus::make_field<int>(num_scheme.sites);
  nb_flux_by_sites.fill(0);
  // for this test, to avoid to identify the cells in the flux tips
  // the distance, cells_volumes, permeability, th_conductivity must be equals
  CHECK((Lx / Nx == Ly / Ny && Lx / Nx == Lz / Nz));
  // CHECK(permeability == permeability(0));
  // CHECK(th_conductivity == th_conductivity(0));
  auto cell_center_dist = Lx / Nx;
  auto face_surf = std::pow(Lx / Nx, 2);
  auto face_meas = mesh.get_face_measures();
  for (auto &&m : face_meas)
    CHECK(fabs(m - face_surf) < eps);
  auto ref_Dt = permeability(0)(0, 0) * face_surf / cell_center_dist;
  auto ref_Ft = th_conductivity(0)(0, 0) * face_surf / cell_center_dist;

  auto si = 0;
  for (auto &&[tips, sten] : std::views::zip(num_scheme.flux_tips.targets(),
                                             num_scheme.stencils.targets())) {
    auto &&[s0, s1] = tips;
    auto &&[st0, st1] = sten;
    CHECK((s0 == st0 && s1 == st1));
    nb_flux_by_sites(s0) += 1;
    nb_flux_by_sites(s1) += 1;
    // if s0 is a cell site
    if (std::find(sites_Ki.begin(), sites_Ki.end(), s0) != sites_Ki.end()) {
      // if s1 is a cell site
      if (std::find(sites_Ki.begin(), sites_Ki.end(), s1) != sites_Ki.end()) {
        check_tpfa_trans(si, Darcy_trans, Fourier_trans, ref_Dt, ref_Ft);
      } else {
        // check that s1 is a face
        CHECK(std::find(sites_Fi.begin(), sites_Fi.end(), s1) !=
              sites_Fi.end());
        check_tpfa_trans(si, Darcy_trans, Fourier_trans, ref_Dt * 2.0,
                         ref_Ft * 2.0);
      }
    } else {
      // check that s0 is a face and s1 is a cell
      CHECK(std::find(sites_Fi.begin(), sites_Fi.end(), s0) != sites_Fi.end());
      CHECK(std::find(sites_Ki.begin(), sites_Ki.end(), s1) != sites_Ki.end());
      check_tpfa_trans(si, Darcy_trans, Fourier_trans, ref_Dt * 2.0,
                       ref_Ft * 2.0);
    }
    si += 2; // size of the stencil
  }

  // each cell is involved in 3 (or 4) flux computation, dir faces in 1
  auto nb_bound_K = 0;
  auto nb_flux_by_cells = nb_flux_by_sites.extract(sites_K);
  for (auto &&i : nb_flux_by_cells) {
    CHECK((i == 3 or i == 4));
    if (i == 4)
      nb_bound_K += 1;
  }
  CHECK((std::size_t)nb_bound_K == dirichlet.size());
  CHECK(nb_flux_by_sites.extract(sites_F) == 1);

  return std::tuple(num_scheme, Darcy_trans, Fourier_trans);
}

template <std::size_t scheme_dim, typename Mesh>
static auto vag_scheme(auto &mesh, auto scheme_cells, auto permeability,
                       auto th_conductivity) {
  // init the numerical scheme
  auto num_scheme = numscheme::VAG<Mesh, scheme_dim>(mesh, scheme_cells);
  // if the scheme is applied on faces, change the thickness
  if constexpr (scheme_dim == 2)
    num_scheme.cells_thickness.fill(0.1);
  auto scheme_nodes = mesh.template get_connectivity<scheme_dim, mesh.node>()
                          .extract(scheme_cells, mesh.nodes)
                          .image();
  CHECK(scheme_nodes == num_scheme.build_info._vag_nodes);
  CHECK(num_scheme.sites.size() == scheme_cells.size() + scheme_nodes.size());
  // Dirichlet : top nodes (VAG)
  auto top_nodes = get_close_to<mesh.node>(mesh, mesh.dimension - 1, 1.e0);
  // for VAG2D in a 3D mesh, select the top nodes of the VAG nodes
  // otherwise, top nodes are included in vag nodes
  auto dirichlet = icus::intersect(top_nodes, scheme_nodes, mesh.nodes);
  if constexpr (mesh.dimension == 3) {
    auto [Nx, Ny, Nz] = *mesh.shape;
    if constexpr (scheme_dim == 3) {
      CHECK(scheme_nodes.size() == mesh.nodes.size());
      CHECK(dirichlet.size() == (Nx + 1) * (Ny + 1));
    }
    if constexpr (scheme_dim == 2) {
      // VAG applied on the plan x=0.5
      CHECK(scheme_nodes.size() == (Ny + 1) * (Nz + 1));
      CHECK(dirichlet.size() == Ny + 1);
    }
  } else if constexpr (mesh.dimension == 2) {
    auto [Nx, Nz] = *mesh.shape;
    CHECK(dirichlet.size() == Nx + 1);
  }
  // test distribute_volumes
  auto vol_qt = icus::Field(scheme_cells);
  std::srand(std::time(0)); // use current time as seed for random generator
  for (auto &&v : vol_qt)
    v = std::rand();
  // mimic the rocktypes
  auto sites_label = icus::Labels(num_scheme.sites);
  sites_label.fill(0);
  // is_volume_nodes is carried by mesh.nodes
  auto &&is_volume_nodes = is_vol_nodes(mesh.nodes, dirichlet);
  auto omega = 0.075;
  if constexpr (scheme_dim == 2)
    omega = 0.15;
  auto sites_qt = num_scheme.distribute_volumes(vol_qt, sites_label,
                                                is_volume_nodes, omega);
  vag_check_sites_qt(num_scheme, dirichlet, vol_qt, sites_qt);

  // init Darcy and Fourier Transmissivities
  auto Darcy_trans = num_scheme.build_transmissivities(permeability);
  auto Fourier_trans = num_scheme.build_transmissivities(th_conductivity);
  // // complete test ???

  return std::make_tuple(Darcy_trans, Fourier_trans);
}

template <std::size_t mesh_dim = 3>
static auto build_diff_tensor(const auto &cells, auto perm_val,
                              auto thermcond_val) {
  using Tensor = numscheme::Tensor_d<mesh_dim>;
  using Tensor_field = numscheme::Tensor_field<mesh_dim>;
  // careful, carried by the cells, Tensor
  auto permeability = Tensor_field(cells);
  auto perm = Tensor({0.0});
  for (std::size_t i = 0; i < mesh_dim; ++i)
    perm(i, i) = perm_val;
  permeability.fill(perm);
  auto thermal_conductivity = Tensor_field(cells);
  auto th_cond = Tensor({0.0});
  for (std::size_t i = 0; i < mesh_dim; ++i)
    th_cond(i, i) = thermcond_val; // W/m/K
  thermal_conductivity.fill(th_cond);

  return std::make_tuple(permeability, thermal_conductivity);
}

TEST_CASE("basic num scheme") {
  SECTION("TPFA over a 3D mesh") {
    ////////////////////////////////////////////////////////////
    //                    TPFA over a 3D mesh                //
    ////////////////////////////////////////////////////////////
    // creates a 3D grid with, at each direction: N segments and N+1 nodes from
    // 0 to 1 tpfa test is valid only with N=2 (3 or 4 fluxes involved by sites)
    std::size_t N = 2;
    auto tpfa_mesh = build_3Dmesh(N);
    auto &&[permeability, thermal_conductivity] =
        build_diff_tensor(tpfa_mesh.cells, 1.e-12, 3.0);

    auto [scheme, DT, FT] = tpfa_scheme(tpfa_mesh, tpfa_mesh.cells,
                                        permeability, thermal_conductivity);

    // test of LFVS_flux ?????
    // test of LFVS_gravity ?????
  }
  SECTION("VAG (3D and 2D in a 3D mesh and 2D in 2D mesh") {
    ////////////////////////////////////////////////////////////
    //                     VAG3D in a 3D mesh                 //
    ////////////////////////////////////////////////////////////
    using raw_mesh_3 =
        icmesh::Raw_mesh<3, icmesh::structured_connectivity_matrix<3>>;
    using Mesh_3s = icmesh::Geomtraits<raw_mesh_3>;
    // use even numbers to identify the faces thanks to the face center
    // center.x close to 0.5, to avoid to identify 2 planes
    auto N = 20;
    auto vag_3Dmesh = build_3Dmesh(N);
    auto &&[perm, thermal_cond] =
        build_diff_tensor(vag_3Dmesh.cells, 1.e-12, 3.0);
    vag_scheme<3, Mesh_3s>(vag_3Dmesh, vag_3Dmesh.cells, perm, thermal_cond);

    ////////////////////////////////////////////////////////////
    //                     VAG2D in a 3D mesh                 //
    ////////////////////////////////////////////////////////////
    // apply the VAG scheme over a set of faces of the 3D mesh, plan x=0.5
    auto middle_faces = get_close_to<vag_3Dmesh.face>(vag_3Dmesh, 0, 5.0e-1);
    CHECK(middle_faces.size() == N * N);
    auto vag2D_perm = numscheme::Scalar_field(middle_faces);
    vag2D_perm.fill(1.e-12);
    auto vag2D_thermcond = numscheme::Scalar_field(middle_faces);
    vag2D_thermcond.fill(3.0);
    auto &&[Darcy2D3D, Fourier2D3D] = vag_scheme<2, Mesh_3s>(
        vag_3Dmesh, middle_faces, vag2D_perm, vag2D_thermcond);

    ////////////////////////////////////////////////////////////
    //                     VAG2D in a 2D mesh                 //
    ////////////////////////////////////////////////////////////
    using raw_mesh_2 =
        icmesh::Raw_mesh<2, icmesh::structured_connectivity_matrix<2>>;
    using Mesh_2s = icmesh::Geomtraits<raw_mesh_2>;
    auto step = icmesh::Constant_axis_offset{0., (std::size_t)N, 1. / N};
    auto vag_2Dmesh = icmesh::mesh(icmesh::lattice(step, step));
    auto vag2Dmesh2D_perm = numscheme::Scalar_field(vag_2Dmesh.cells);
    vag2Dmesh2D_perm.fill(1.e-12);
    auto vag2Dmesh2D_thermcond = numscheme::Scalar_field(vag_2Dmesh.cells);
    vag2Dmesh2D_thermcond.fill(3.0);
    auto &&[Darcy2D2D, Fourier2D2D] = vag_scheme<2, Mesh_2s>(
        vag_2Dmesh, vag_2Dmesh.cells, vag2Dmesh2D_perm, vag2Dmesh2D_thermcond);

    // check that the transmissivities computed with VAG2D in the 2D or 3D mesh
    // are equals
    auto eps = 1.e-10;
    for (auto &&[T0, T1] : std::views::zip(Darcy2D2D, Darcy2D3D))
      CHECK(fabs(T1 - T0) < eps);

    for (auto &&[T0, T1] : std::views::zip(Fourier2D2D, Fourier2D3D))
      CHECK(fabs(T1 - T0) < eps);
  }
}
