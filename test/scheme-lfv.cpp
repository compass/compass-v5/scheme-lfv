#include <catch2/catch_test_macros.hpp>
#include <icmesh/Geomtraits.h>
#include <scheme_lfv/scheme-lfv.h>
#include <scheme_lfv/tpfa.h>
#include <scheme_lfv/vag.h>

template <std::size_t elem_i>
static auto get_close_to(auto &mesh, auto dir, auto x, double eps = 1.e-6) {
  auto centers = mesh.template get_isobarycenters<elem_i>();
  std::vector<icus::index_type> top_indices;
  for (auto &&[i, v] : centers.enumerate())
    if (abs(v[dir] - x) < eps)
      top_indices.push_back(i);

  return mesh.elements[elem_i].extract(top_indices);
}

TEST_CASE("VAG2D in a 3D mesh") {
  // test the 2D VAG scheme in a 3D domain
  using raw_mesh_3 =
      icmesh::Raw_mesh<3, icmesh::structured_connectivity_matrix<3>>;
  using Mesh_3s = icmesh::Geomtraits<raw_mesh_3>;

  // Constant_axis_offset{origin, Nb_elements, dx}
  auto x_step = icmesh::Constant_axis_offset{-1., 2, 1.};
  auto yz_step = icmesh::Constant_axis_offset{-0.5, 1, 1.};
  auto mesh = icmesh::mesh(icmesh::lattice(x_step, yz_step, yz_step));
  // apply the VAG scheme over the uniq face of the mesh where x=0.
  auto middle_face = get_close_to<mesh.face>(mesh, 0, 0.0);
  // init the numerical scheme
  auto num_scheme = numscheme::VAG<Mesh_3s, 2>(mesh, middle_face);
  num_scheme.cells_thickness.fill(1.0);
  auto perm_value = 1.e-12;
  auto permeability = numscheme::Scalar_field(middle_face);
  permeability.fill(perm_value);
  auto Darcy_trans = num_scheme.build_transmissivities(permeability);
  // test the values of the transmissivities
  // Tks(k) = -perm_value; Tks(s') = perm_value
  for (auto &&[flux, stencil] : num_scheme.stencils.targets_by_source()) {
    auto &&[s0, s1] = num_scheme.flux_tips.targets_by_source(flux);
    auto &&conn = num_scheme.stencils.connections_by_source(flux);
    double sum = 0.;
    for (auto &&[s, i] : std::views::zip(stencil, conn)) {
      sum += Darcy_trans(i);
      if (s == s0 || s == s1)
        CHECK(fabs(fabs(Darcy_trans(i)) - perm_value) < 1.e-7);
    }
    CHECK(fabs(sum < 1.e-7));
  }
}
