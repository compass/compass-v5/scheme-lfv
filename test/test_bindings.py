from scheme_lfv import Sites
from icus import ISet
import numpy as np


def test_bindings():
    N = 12
    cells = ISet(N)
    sites0 = Sites(ISet(N))
    sites1 = Sites(cells)
    for i in cells:
        print(i)
    for i in sites1:
        print(i)
    mapping = [0, 1, 3, 5]
    sub_cells = cells.extract(mapping)
    assert sites0.is_consistent()
    assert sites1.is_consistent()
    sub_sites = sites1.from_location(sub_cells)
    assert np.all(sub_sites.mapping_as_array() == mapping)
    sites1.to_location(sites1)
