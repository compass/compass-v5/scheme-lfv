cmake_minimum_required(VERSION 3.24)
cmake_policy(VERSION 3.24)

project(scheme-lfv-tests LANGUAGES CXX)

execute_process(
  COMMAND compass use-compass-file
  OUTPUT_VARIABLE USE_COMPASS
  OUTPUT_STRIP_TRAILING_WHITESPACE)

include(${USE_COMPASS})

find_package(scheme-lfv REQUIRED)
find_package(icmesh REQUIRED)

compass_add_cpp_catch2tests(FILES scheme-lfv.cpp basic-num-scheme.cpp PRIVATE
                            icmesh scheme-lfv)
