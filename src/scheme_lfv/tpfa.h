#pragma once

#include <cassert>
#include <icus/Connectivity.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <scheme_lfv/scheme-lfv.h>
#include <set>
#include <variant>

namespace numscheme {

template <typename Mesh_t>
using TPFA_Base = LFV_scheme<Mesh_t, Connectivity<ISet, ISet, 2>,
                             Connectivity<ISet, ISet, 2>>;

// tpfa scheme (no fracture)
template <typename Mesh_t> struct TPFA : TPFA_Base<Mesh_t> {

  struct Builder {
    // contains the necessary info to build the scheme
    // we keep an instance in the scheme, which could be deleted
    // when it is not necessary anymore
    struct Interior_flux {
      Scalar face_area;
      Scalar dK, dL;
      Mesh_t::Vertex nf;
    };
    struct Boundary_flux {
      Scalar face_area;
      Scalar dK;
      Mesh_t::Vertex nf;
    };
    using Variant_flux = std::variant<Interior_flux, Boundary_flux>;

    // cells connecting faces, when the cells are in this->cells
    Connectivity<ISet> _faces_cells;
    Connectivity<ISet, ISet, 1> _bound_faces_cells;
    // each flux needs informations from one face
    Field<Variant_flux> _flux_info;
  };

  // contains the necessary info to build the scheme, can be deleted after
  Builder build_info;

  TPFA(Mesh_t &the_mesh, ISet tpfa_cells, ISet tpfa_bound_faces)
      : TPFA_Base<Mesh_t>(the_mesh) {
    // or this->cells = tpfa_cells; ???
    auto &&mesh = this->mesh;
    if (!tpfa_cells.is_included_in(mesh.cells)) {
      throw std::runtime_error(
          "The TPFA cells are not included in the mesh cells!");
    }
    // the following if can be very costly !
    // if same pointer, can be quick,
    // if different sizes, also quick
    if (tpfa_cells == mesh.cells) {
      this->cells = tpfa_cells;
    } else {
      this->cells = tpfa_cells.rebase(mesh.cells);
    }

    // get connectivity, face.parent and cell.parent are mesh.joined_elements
    auto mesh_faces_cells =
        mesh.template get_connectivity<mesh.face, mesh.cell>();

    // call restrict_target with simplify_source = true
    // means that _faces_cells.source() is an iset extracted
    // from mesh_faces_cells.source() where unused faces have been removed
    // _faces_cells.target() is this->cells
    build_info._faces_cells =
        mesh_faces_cells.restrict_target(this->cells, true);

    // make a partition of the connectivity with interior_faces_cells
    // and boundary_faces_cells
    // identify boundary faces
    std::vector<int> boundary_faces_index;
    for (auto &&[f, neigh_c] : build_info._faces_cells.targets_by_source()) {
      if (neigh_c.size() != 2) {
        boundary_faces_index.push_back(f);
      }
    }
    // partition of faces
    auto boundary_faces =
        build_info._faces_cells.source().extract(boundary_faces_index);
    if (!tpfa_bound_faces.is_included_in(boundary_faces)) {
      throw std::runtime_error("The TPFA boundary faces are not included in "
                               "the mesh boundary faces!");
    }
    auto int_faces = build_info._faces_cells.source().substract(boundary_faces);
    // careful int_faces_cells and build_info._bound_faces_cells are not a
    // partition of build_info._faces_cells because only the boundary faces
    // carrying a site are kept useful connectivity concerning interior faces
    auto int_faces_cells = build_info._faces_cells.template extract<2>(
        int_faces, build_info._faces_cells.target());
    // useful connectivity concerning tpfa boundary faces
    // all boundary faces are not necessary, some of them (Neumann for exemple)
    // do not carry sites
    build_info._bound_faces_cells = build_info._faces_cells.template extract<1>(
        tpfa_bound_faces, build_info._faces_cells.target());

    // tmp_flux_tips contains the tips in mesh index
    auto &&[tmp_flux_tips, tmp_flux_info] =
        init_std_faces(int_faces_cells, build_info._bound_faces_cells);
    // init sites, flux_tips (after conversion from mesh indices to
    // sites indices), flux_info and stencils
    init_tpfa_LFV_scheme(mesh.joined_elements, tmp_flux_tips, tmp_flux_info);
  }

  Sites mesh2sites(const ISet &mesh_iset,
                   std::vector<index_type> &vect_flux_tips) {
    auto only_once =
        std::set<index_type>(vect_flux_tips.begin(), vect_flux_tips.end());
    for (auto &&tip : vect_flux_tips) {
      auto site_index = only_once.find(tip);
      assert(site_index != only_once.end());
      tip = std::distance(only_once.begin(), site_index);
    }
    return Sites(mesh_iset.extract(only_once));
  }

  void init_tpfa_LFV_scheme(
      const ISet &mesh_iset, std::vector<index_type> &vect_flux_tips,
      std::vector<typename Builder::Variant_flux> &vect_flux_info) {
    // np.uniq on vect_flux_tips values gives the sites cardinal and mapping
    // to mesh_iset. Site object means that sites is an iset without parent yet
    // and sites.location.parent() = mesh_iset.
    // mesh2sites also modify vect_flux_tips to have tips in sites index
    this->sites = mesh2sites(mesh_iset, vect_flux_tips);

    assert(vect_flux_tips.size() / 2 == vect_flux_info.size());
    auto fluxes = ISet{vect_flux_info.size()};
    std::vector<Pair> neighbours;
    neighbours.reserve(vect_flux_info.size());
    for (int i = 0; i < vect_flux_tips.size(); ++i) {
      neighbours.push_back({vect_flux_tips[i], vect_flux_tips[++i]});
    }
    // build connectivity with source = fluxes iset; target = sites iset,
    // preserve_targets_order = true
    this->flux_tips = {fluxes, this->sites, neighbours, true};
    // std::cout << "in neighbours " << std::endl;
    // for (auto &&[n0, n1] : neighbours) {
    //   std::cout << n0 << ", " << n1 << std::endl;
    // }
    // std::cout << "in flux_tips " << std::endl;
    // for (auto &&[s0, s1] : flux_tips.targets()) {
    //   std::cout << s0 << ", " << s1 << std::endl;
    //   // if assert is not true anymore, change in transmissivities s0 and s1
    //   assert(s0 < s1);
    // }

    // build field with source = fluxes iset
    build_info._flux_info = {fluxes};
    for (auto &&[i, info] : std::views::enumerate(vect_flux_info)) {
      build_info._flux_info(i) = info;
    }
    // in TPFA, stencils = flux_tips
    this->stencils = this->flux_tips;
  }

  // init the necessary vectors to init the LFV scheme attributs
  // when int_faces_cells contains the connectivity of interior standard faces
  // (no fracture, no bound) and bound_faces_cells contains the connectivity
  // of boundary standard faces (no fracture no interior)
  // warning, Variant due to Interior_flux or Boundary_flux
  auto init_std_faces(const Connectivity<ISet, ISet, 2> &int_faces_cells,
                      const Connectivity<ISet, ISet, 1> &bound_faces_cells) {
    std::vector<index_type> tmp_flux_tips; // will be vect of Pair later
    // variant because Cell - Cell  or Cell - Face
    std::vector<typename Builder::Variant_flux> tmp_flux_info;

    // ----------------------- Interior faces -----------------------
    // flux Cell - Cell : CC
    fill_CC_fluxes(int_faces_cells, tmp_flux_tips, tmp_flux_info);
    // ----------------------- Boundary faces -----------------------
    // flux Cell - Boundary Face : CboundF
    fill_CboundF_fluxes(bound_faces_cells, tmp_flux_tips, tmp_flux_info);

    assert(tmp_flux_tips.size() / 2 == tmp_flux_info.size());

    return std::make_tuple(tmp_flux_tips, tmp_flux_info);
  }

  // int_faces_cells must contains interior faces only,
  // no fracture face, no boundary face
  void fill_CC_fluxes(const Connectivity<ISet, ISet, 2> &int_faces_cells,
                      std::vector<index_type> &tips,
                      std::vector<typename Builder::Variant_flux> &conn_info) {
    auto &&mesh = this->mesh;
    auto std_int_faces = int_faces_cells.source();
    auto cells = int_faces_cells.target();

    auto int_faces_nodes =
        mesh.template get_connectivity<mesh.face, mesh.node>().extract(
            std_int_faces, mesh.nodes);

    auto rebased_cells = cells.rebase(mesh.joined_elements);
    auto cells_center =
        mesh.template get_isobarycenters<mesh.cell>().extract(cells);
    auto std_faces_center =
        mesh.template get_isobarycenters<mesh.face>().extract(std_int_faces);
    auto std_faces_measure =
        mesh.template get_measures<mesh.face>().extract(std_int_faces);

    auto f = 0;
    for (auto &&[K, L] : int_faces_cells.targets()) {
      auto &&xF = std_faces_center(f);
      auto &&f_nodes = int_faces_nodes.targets_by_source(f);

      // Do not separate the filling of conn_info and tips,
      // the indices must coincide between conn_info and stencils.source()
      // The computation of transmissivities is based on that !
      conn_info.emplace_back(typename Builder::Interior_flux{
          std_faces_measure(f), icmesh::distance(cells_center(K), xF),
          icmesh::distance(cells_center(L), xF),
          icmesh::normal_vect<Mesh_t::dimension>(mesh.vertices, f_nodes)});
      // store tips indices wrt mesh indices
      // store indices in flat vector, after uniq will be united in a Pair
      // struct
      tips.push_back(rebased_cells.mapping(K));
      tips.push_back(rebased_cells.mapping(L));
      ++f;
    }
  }

  // bound_faces_cells must contains boundary faces only,
  // no fracture face, no interior face
  void
  fill_CboundF_fluxes(const Connectivity<ISet, ISet, 1> &bound_faces_cells,
                      std::vector<index_type> &tips,
                      std::vector<typename Builder::Variant_flux> &conn_info) {

    if (bound_faces_cells.connections().empty())
      return;
    auto &&mesh = this->mesh;
    auto std_bound_faces = bound_faces_cells.source();
    auto cells = bound_faces_cells.target();

    auto bound_faces_nodes =
        mesh.template get_connectivity<mesh.face, mesh.node>().extract(
            std_bound_faces, mesh.nodes);

    // done twice
    auto rebased_cells = cells.rebase(mesh.joined_elements);
    auto rebased_std_faces = std_bound_faces.rebase(mesh.joined_elements);
    auto cells_center =
        mesh.template get_isobarycenters<mesh.cell>().extract(cells);
    auto std_faces_center =
        mesh.template get_isobarycenters<mesh.face>().extract(std_bound_faces);
    auto std_faces_measure =
        mesh.template get_measures<mesh.face>().extract(std_bound_faces);

    auto f = 0;
    for (auto &&[K] : bound_faces_cells.targets()) {
      auto xF = std_faces_center(f);
      auto &&f_nodes = bound_faces_nodes.targets_by_source(f);

      // Do not separate the filling of conn_info and tmp_flux_tips,
      // the indices must coincide between
      // flux_info and stencils.source()
      // The computation of transmissivities is based on that !
      conn_info.emplace_back(typename Builder::Boundary_flux{
          std_faces_measure(f), icmesh::distance(cells_center(K), xF),
          icmesh::normal_vect<Mesh_t::dimension>(mesh.vertices, f_nodes)});
      // store tips indices wrt mesh
      // store indices in flat vector
      tips.push_back(rebased_cells.mapping(K));
      tips.push_back(rebased_std_faces.mapping(f));
      ++f;
    }
  }

  template <typename lambda_t> lambda_t build_lambda(lambda_t lambda) {
    // in tpfa, lambda of cells only are used in the transmissivity computation
    auto sites_lambda = lambda_t(this->sites);
    auto &&mesh_cells = this->mesh.cells;
    // the following assert is not necessary but the opposite is not
    // implemented yet
    assert(lambda.support.is_included_in(mesh_cells));
    auto rebased_support = lambda.support.rebase(this->sites.location);
    for (auto &&[i, val] : lambda.enumerate()) {
      sites_lambda(rebased_support.mapping(i)) = val;
    }
    return sites_lambda;
  }

  template <typename lambda_t>
  Scalar_field build_transmissivities(lambda_t lambda) {
    auto scheme_lambda = build_lambda(lambda);
    auto trans = Scalar_field(this->stencils.connections());
    std_compute_transmissivities(scheme_lambda, trans, this->stencils);
    return trans;
  }

  template <typename lambda_t>
  void std_compute_transmissivities(lambda_t lambda, Scalar_field trans,
                                    Connectivity<ISet, ISet, 2> std_stencils) {

    auto ti = index_type{0};
    auto flux = index_type{0};
    for (auto &&[s0, s1] : std_stencils.targets()) {

      auto Tf = std::visit(
          [lambda, s0, s1](auto info) {
            using I = std::decay_t<decltype(info)>;
            if constexpr (std::is_same_v<I, typename Builder::Interior_flux>) {
              auto &&[face_area, ds0, ds1, nf] = info;
              return compute_trans(face_area, ds0, lambda(s0), ds1, lambda(s1),
                                   nf);
            }
            if constexpr (std::is_same_v<I, typename Builder::Boundary_flux>) {
              auto &&[face_area, ds0, nf] = info;
              return compute_trans(face_area, ds0, lambda(s0), nf);
            }
          },
          build_info._flux_info(flux));

      trans(ti++) = Tf;
      trans(ti++) = -Tf;
      ++flux;
    }
    assert(ti == std_stencils.connections().size() &&
           "error in compute_transmissivities");
  }

  // // cell - cell transmissivity with scalar perm
  // template <typename Vector>
  // static inline Scalar compute_trans(Scalar face_area, Scalar ds0, Scalar
  // lambda0, Scalar ds1, Scalar lambda1, Vector nf) {
  //   // face_area/dist(K, L) * lambda_sigma
  //   // harmonic mean
  //   // lambda_sigma = dist(K, L) / (ds0/lambda0 + ds1/lambda1)
  //   return face_area / (ds0 / lambda0 + ds1 / lambda1);
  // }

  // // face - cell transmissivity with scalar perm
  // template <typename Vector>
  // static inline Scalar compute_trans(Scalar face_area, Scalar ds0, Scalar
  // lambda0, Vector nf) {
  //   // face_area/ds0 * lambda_sigma
  //   // lambda_sigma = lambda0
  //   return face_area / ds0 * lambda0;
  // }

  // compute (lambda * n) scalar n
  template <typename Tensor, typename Vector>
  static inline Scalar lambda_n_n(Tensor lambda, Vector nf) {
    Vector lambda_n;
    lambda_n[0] =
        lambda(0, 0) * nf[0] + lambda(0, 1) * nf[1] + lambda(0, 2) * nf[2];
    lambda_n[1] =
        lambda(1, 0) * nf[0] + lambda(1, 1) * nf[1] + lambda(1, 2) * nf[2];
    lambda_n[2] =
        lambda(2, 0) * nf[0] + lambda(2, 1) * nf[1] + lambda(2, 2) * nf[2];

    return std::inner_product(lambda_n.begin(), lambda_n.end(), nf.begin(),
                              0.0);
  }

  // cell - cell transmissivity with tensor perm
  template <typename Tensor, typename Vector>
  static inline Scalar compute_trans(Scalar face_area, Scalar ds0,
                                     Tensor lambda0, Scalar ds1, Tensor lambda1,
                                     Vector nf) {
    // face_area/dist(K, L) * lambda_sigma
    // lambda_sigma = dist(K, L) / (ds0/(lambda0 nf scalar nf) + ds1/(lambda1 nf
    // scalar nf)) it is supposed that (lambda nf) and tf = (x_K - x_L) are
    // colinear
    auto oriented_lambda0 = lambda_n_n(lambda0, nf);
    auto oriented_lambda1 = lambda_n_n(lambda1, nf);
    return face_area / (ds0 / oriented_lambda0 + ds1 / oriented_lambda1);
  }

  // face - cell transmissivity with tensor perm
  template <typename Tensor, typename Vector>
  static inline Scalar compute_trans(Scalar face_area, Scalar ds, Tensor lambda,
                                     Vector nf) {
    // face_area/dist(K, L) * lambda_sigma
    // lambda_sigma = (lambda nf) scalar nf
    // it is supposed that (lambda nf) and tf = (x_K - x_sigma) are colinear
    auto oriented_lambda = lambda_n_n(lambda, nf);
    return face_area / ds * oriented_lambda;
  }

  // distribute volume quantities over sites
  template <typename Quantity_t>
  Quantity_t distribute_volumes(Quantity_t quantities) {
    auto sites_quantities = Quantity_t{this->sites};
    sites_quantities.fill(0.0);
    auto tpfa_cells_quantities = quantities.extract(this->cells);
    auto rebased_cells = this->cells.rebase(this->sites.location);
    for (auto &&[c, quant] :
         std::views::zip(rebased_cells.mapping(), tpfa_cells_quantities))
      sites_quantities(c) = quant;
    return sites_quantities;
  }

  // distribute surface quantities over sites
  // used to distribute the Neumann flux written on faces over
  // the neighbouring sites
  template <typename Quantity_t>
  Field<Quantity_t> distribute_surfaces(Field<Quantity_t> quantities) {
    auto sites_quantities = Field<Quantity_t>(this->sites);
    sites_quantities.fill(Quantity_t{0.0});

    // if there is nothing to distribute, return field with 0 values
    if (quantities.size() == 0)
      return sites_quantities;

    auto &&mesh = this->mesh;
    // the support of the quantities must be a set of faces
    assert(quantities.support.is_included_in(mesh.faces));
    // here faces are necessarily boundary faces, thus only one cell connecting
    auto input_faces_cells = build_info._faces_cells.template extract<1>(
        quantities.support,
        this->cells // target of _faces_cells
    );

    // get support measures
    auto input_faces_measures =
        mesh.get_face_measures().extract(quantities.support);

    // mapping between tpfa cell indices and their site indices
    auto tpfa_cells_sites = this->cells.rebase(this->sites.location);

    for (auto &&[input, face_meas, cell_i] : std::views::zip(
             quantities, input_faces_measures, input_faces_cells.targets())) {
      // cell_i belongs to this->cells, get its site index
      auto cell_site_i = tpfa_cells_sites.mapping(cell_i[0]);
      // input is surfacic, multiply by its measure
      sites_quantities(cell_site_i) += input * face_meas;
    }

    return sites_quantities;
  }
};

} // namespace numscheme
