#pragma once

#include <icus/binding/Connectivity.h>
#include <nanobind/nanobind.h>
#include <scheme_lfv/scheme-lfv.h>
#include <scheme_lfv/tpfa.h>
#include <scheme_lfv/vag.h>

// the bindings defined here are templated by the mesh
// then they are defined here and called later, when the mesh
// is known (in demo-heat-flux, compass-coats...)
namespace numscheme {
namespace bindings {

namespace nb = nanobind;

template <typename FV_scheme> void bind_scheme_lfv(auto &cls) {
  cls.def(nb::init<const FV_scheme &>());
  cls.def_ro("mesh", &FV_scheme::mesh);
  cls.def_ro("sites", &FV_scheme::sites);
  cls.def_ro("flux_tips", &FV_scheme::flux_tips);
  cls.def_ro("stencils", &FV_scheme::stencils);
}

// the transmissivity can be scalar or tensor fields
template <typename FV_scheme, typename Trans_t>
void commun_scheme_bindings(auto &cls) {
  using Comp_vect_field = Field<Tensor<Scalar, physics::nb_components>>;
  // when diffusion factor is a TENSOR
  cls.def("build_transmissivities",
          &FV_scheme::template build_transmissivities<Trans_t>);
  // distribute_volumes is called over scalar field and component vector field
  cls.def("distribute_volumes",
          &FV_scheme::template distribute_volumes<Scalar_field>);
  cls.def("distribute_volumes",
          &FV_scheme::template distribute_volumes<Comp_vect_field>);
  cls.def("distribute_surfaces",
          &FV_scheme::template distribute_surfaces<physics::Accumulation>);
}

template <typename Mesh_t>
void bind_tpfa(nb::module_ &module, auto &&scheme_name) {
  using Tensor_field = Tensor_field<Mesh_t::dimension>;
  auto base_cls_name = scheme_name + std::string("_Base");
  auto base_cls = nb::class_<TPFA_Base<Mesh_t>>(module, base_cls_name.c_str());
  bind_scheme_lfv<TPFA_Base<Mesh_t>>(base_cls);

  auto cls = nb::class_<TPFA<Mesh_t>, TPFA_Base<Mesh_t>>(
      module, scheme_name, "TPFA numerical scheme");
  cls.def(nb::init<const TPFA<Mesh_t> &>());
  cls.def(nb::init<Mesh_t &, ISet, ISet>());
  cls.def_prop_ro("bound_faces_cells",
                  [](const TPFA<Mesh_t> &self) -> icus::binding::Connectivity {
                    return self.build_info._bound_faces_cells;
                  });
  commun_scheme_bindings<TPFA<Mesh_t>, Tensor_field>(cls);
  // constructor to overload the mesh type, call make_tpfa from python
  module.def("make_tpfa", [](Mesh_t &m, ISet cells, ISet dir) {
    return TPFA<Mesh_t>(m, cells, dir);
  });
}

// careful to bind only once Base !
template <typename Mesh_t>
void bind_vag_base(nb::module_ &module, auto &&scheme_name) {
  auto base_cls_name = scheme_name + std::string("_Base");
  auto base_cls = nb::class_<VAG_Base<Mesh_t>>(module, base_cls_name.c_str());
  bind_scheme_lfv<VAG_Base<Mesh_t>>(base_cls);
}

template <typename Mesh_t, std::size_t scheme_dim = Mesh_t::dimension>
void bind_vag(nb::module_ &module, auto &&scheme_name, auto &&scheme_postfix) {
  auto cls = nb::class_<VAG<Mesh_t, scheme_dim>, VAG_Base<Mesh_t>>(
      module, scheme_name, "VAG numerical scheme");
  cls.def(nb::init<const VAG<Mesh_t, scheme_dim> &>());
  cls.def(nb::init<Mesh_t &, ISet>());
  // constructor to overload the mesh type, call make_vag from python
  auto name = std::string("make_vag") + scheme_postfix;
  module.def(name.c_str(), [](Mesh_t &m, ISet cells) {
    return VAG<Mesh_t, scheme_dim>(m, cells);
  });
  cls.def_prop_ro(
      "cells_nodes",
      [](const VAG<Mesh_t, scheme_dim> &self) -> icus::binding::Connectivity {
        return self.build_info._cells_nodes;
      });
  cls.def_ro("cells_thickness", &VAG<Mesh_t, scheme_dim>::cells_thickness);

  if constexpr (scheme_dim < Mesh_t::dimension) {
    // if scheme dim < mesh dim, the anisotropy axes would be lost
    // then it is only possible to use scalar permeabilities, conductivities
    cls.def(
        "build_cells_labels",
        &VAG<Mesh_t, scheme_dim>::template build_cells_labels<Scalar_field>);
    cls.def(
        "build_sites_labels",
        &VAG<Mesh_t, scheme_dim>::template build_sites_labels<Scalar_field>);
    commun_scheme_bindings<VAG<Mesh_t, scheme_dim>, Scalar_field>(cls);
  } else {
    // with tensor permeabilities, conductivities
    using Tensor_field = Tensor_field<Mesh_t::dimension>;
    cls.def(
        "build_cells_labels",
        &VAG<Mesh_t, scheme_dim>::template build_cells_labels<Tensor_field>);
    cls.def(
        "build_sites_labels",
        &VAG<Mesh_t, scheme_dim>::template build_sites_labels<Tensor_field>);
    commun_scheme_bindings<VAG<Mesh_t, scheme_dim>, Tensor_field>(cls);
  }
}

} // namespace bindings
} // namespace numscheme
