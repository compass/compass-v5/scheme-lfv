#pragma once

#include <compass_cxx_utils/array_utils.h>
#include <scheme_lfv/scheme-lfv.h>

namespace numscheme {
// allow array operations
using namespace compass::utils::array_operators;

// Inverse of A
// todo: why do we implement it by ourself ???
// Use something already existing (eigen ???)
template <std::size_t dim = 3, typename Tensor = icus::Tensor<double, dim, dim>>
  requires(dim == 3)
static inline Tensor Inv_tensor(const Tensor &A) {
  Tensor Ainv, coA;
  double det;

  det = A(0, 0) * (A(2, 2) * A(1, 1) - A(2, 1) * A(1, 2));
  det = det - A(0, 1) * (A(2, 2) * A(1, 0) - A(2, 0) * A(1, 2));
  det = det + A(0, 2) * (A(2, 1) * A(1, 0) - A(2, 0) * A(1, 1));

  coA(0, 0) = +(A(2, 2) * A(1, 1) - A(2, 1) * A(1, 2)) / det;
  coA(0, 1) = -(A(2, 2) * A(1, 0) - A(2, 0) * A(1, 2)) / det;
  coA(0, 2) = +(A(2, 1) * A(1, 0) - A(2, 0) * A(1, 1)) / det;

  coA(1, 0) = -(A(2, 2) * A(0, 1) - A(2, 1) * A(0, 2)) / det;
  coA(1, 1) = +(A(2, 2) * A(0, 0) - A(2, 0) * A(0, 2)) / det;
  coA(1, 2) = -(A(2, 1) * A(0, 0) - A(2, 0) * A(0, 1)) / det;

  coA(2, 0) = +(A(1, 2) * A(0, 1) - A(1, 1) * A(0, 2)) / det;
  coA(2, 1) = -(A(1, 2) * A(0, 0) - A(1, 0) * A(0, 2)) / det;
  coA(2, 2) = +(A(1, 1) * A(0, 0) - A(1, 0) * A(0, 1)) / det;

  // Ainv = coA transpose
  for (auto i = 0; i < dim; ++i) {
    for (auto j = 0; j < dim; ++j)
      Ainv(i, j) = coA(j, i);
  }

  return Ainv;
}

// Inverse of A
// todo: why do we implement it by ourself ???
// Use something already existing (eigen ???)
template <std::size_t dim, typename Tensor = icus::Tensor<double, dim, dim>>
  requires(dim == 2)
static inline Tensor Inv_tensor(const Tensor &A) {
  Tensor Ainv;
  auto det = A(0, 0) * A(1, 1) - A(0, 1) * A(1, 0);
  assert(fabs(det) > 1.e-12); // det != 0
  Ainv(0, 0) = A(1, 1) / det;
  Ainv(0, 1) = -A(0, 1) / det;
  Ainv(1, 0) = -A(1, 0) / det;
  Ainv(1, 1) = A(0, 0) / det;

  return Ainv;
}

// test the gradient over a linear function
template <typename Mesh_t, std::size_t scheme_dim>
  requires(Mesh_t::dimension == scheme_dim)
bool inline check_GkT(const auto &f_nodes, const auto &xk, const auto &GkT,
                      const Mesh_t &mesh, double eps = 1e-8) {
  bool ok = true;
  // gradient test GkT, the solution is a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
  std::array<double, Mesh_t::dimension + 1> an;
  std::srand(std::time(0)); // use current time as seed for random generator
  for (auto &&ai : an)
    ai = std::rand() % int(100);

  // in 3D u = a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
  auto uk = an[Mesh_t::dimension];
  for (auto i = 0; i < Mesh_t::dimension; ++i)
    uk += an[i] * xk[i];

  // Mesh_t::Vertex is an special type so already init at 0,
  // not true anymore if Mesh_t::Vertex becomes an array
  typename Mesh_t::Vertex sum;

  // nodes
  for (auto &&[in, n] : std::views::enumerate(f_nodes)) {
    auto xn = mesh.vertices(n);

    // in 3D u = a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
    auto un = an[Mesh_t::dimension];
    for (auto i = 0; i < Mesh_t::dimension; ++i)
      un += an[i] * xn[i];
    for (auto i = 0; i < Mesh_t::dimension; ++i)
      sum[i] += GkT[in][i] * (uk - un);
  }

  // compute relative norm of the error
  double err = 0;
  double norm = 0;
  for (auto i = 0; i < Mesh_t::dimension; ++i) {
    err += fabs(sum[i] - an[i]);
    norm += fabs(an[i]);
  }
  if (err / norm > eps) {
    compass::utils::Pretty_printer print;
    print.endl = "\n";

    print("Gradient error: ", err, ", norm ", norm);
    print("Relative error: ", err / norm, ", is compared to ", eps);
    print("   Center of Cell: ", xk);
    ok = false;
  }

  return ok;
}

// Test the **tangential gradient** over a linear function
template <typename Mesh_t, std::size_t scheme_dim>
  requires(Mesh_t::dimension == 3 &&
           scheme_dim == 2) // todo: test with other dimensions
bool inline check_GkT(const auto &k_nodes, const auto &xk, const auto &n,
                      const auto &GkT, const Mesh_t &mesh, double eps = 1e-12) {

  // gradient test GkT, the solution is a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
  std::array<double, Mesh_t::dimension + 1> an;
  std::srand(std::time(0)); // use current time as seed for random generator
  for (auto &&ai : an)
    ai = std::rand() % int(1e3);

  // in 3D u = a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
  auto uk = an[Mesh_t::dimension];
  // ps is the scalar product with the normal vect
  auto ps = 0.0;
  for (auto i = 0; i < Mesh_t::dimension; ++i) {
    uk += an[i] * xk[i];
    ps += an[i] * n[i];
  }

  std::array<double, Mesh_t::dimension> gx;
  // not already init to 0, non class type
  gx.fill(0);

  // nodes
  for (auto &&[in, n] : std::views::enumerate(k_nodes)) {
    auto xn = mesh.vertices(n);

    // in 3D u = a0 * x[0] + a1 * x[1] + a2 * x[2] + a3
    auto un = an[Mesh_t::dimension];
    for (auto i = 0; i < Mesh_t::dimension; ++i)
      un += an[i] * xn[i];
    for (auto i = 0; i < Mesh_t::dimension; ++i)
      gx[i] += GkT[in][i] * (uk - un);
  }

  // compute relative norm of the error
  double err = 0;
  double norm = 0;
  for (auto i = 0; i < Mesh_t::dimension; ++i) {
    err += fabs(gx[i] - (an[i] - ps * n[i]));
    norm += fabs(an[i] - ps * n[i]);
  }

  if (err / norm > eps) {
    compass::utils::Pretty_printer print;
    print.endl = "\n";
    print("an ", an);
    print("Tangential gradient error ", err, ", norm ", norm);
    print("Relative error ", err / norm, ", is compared to ", eps);
    print("   Center of Cell: ", xk);
    return false;
  }

  return true;
}

// build the gradient trans when mesh_t dim = VAG dim = 3
template <typename Mesh_t, std::size_t scheme_dim>
  requires(scheme_dim == 3)
auto inline build_GkT(const Mesh_t &mesh, const auto &f_nodes,
                      const auto &f_nodes_mapping, const auto &xk,
                      const auto &xf, auto n0, auto n1) {
  // edge with node n0, n1
  auto x0 = mesh.vertices(n0);
  auto x1 = mesh.vertices(n1);

  // the subdivision is a tetra
  // volume and center of tetra n0, n1, face center, cell center
  auto volT = icmesh::tetra_volume(x0, x1, xf, xk);
  auto xT = x0 + x1 + xf + xk;
  xT /= 4.0;

  // normal vector to 01k in direction of X and of norm the surface of
  // 01k
  auto &&[v01k, surf_01k] = icmesh::triangle_VecNormalT(x0, x1, xk, xT);
  v01k *= surf_01k;
  // normal vector to 0fk in direction of X and of norm the surface of
  // 0fk
  auto &&[v0fk, surf_0fk] = icmesh::triangle_VecNormalT(x0, xf, xk, xT);
  v0fk *= surf_0fk;
  // normal vector to f1k in direction of X and of norm the surface of
  // f1k
  auto &&[vf1k, surf_f1k] = icmesh::triangle_VecNormalT(xf, x1, xk, xT);
  vf1k *= surf_f1k;

  // Set GkT
  // Grad T_01fk = 1/(3*volT) (v01k(uf-uk) + v0fk(u0-uk) + vf1k(u1-uk))
  // where uf = 1/NbNodebyFace \sum_(n=node of face) u_n
  // todo: new object for each cell face ???
  auto nb_f_nodes = f_nodes.size();
  std::vector<typename Mesh_t::Vertex> GkT(nb_f_nodes);
  // GkT = 0.0; // already init to 0 ???
  // Mesh_t::Vertex is an special type so already init at 0,
  // not true anymore if Mesh_t::Vertex becomes an array
  auto ss = 3.0 * volT;
  vf1k /= ss;
  GkT[f_nodes_mapping(n0)] += vf1k;
  v0fk /= ss;
  GkT[f_nodes_mapping(n1)] += v0fk;
  // ss = 1.0/(3.0*volT*nbNodeFace), then GkT(n) += v01k * ss
  v01k /= 3.0 * volT * nb_f_nodes;
  // loop over the nodes of face f for the contribution 1/(3*volT) V12k
  // (uk-uf)
  for (auto &&[in, n] : std::views::enumerate(f_nodes))
    GkT[in] += v01k;

  assert((check_GkT<Mesh_t, scheme_dim>(f_nodes, xk, GkT, mesh)));

  return std::tuple(GkT, volT);
}

// build the gradient trans when mesh_t dim = 2 or 3 and VAG dim = 2
template <typename Mesh_t, std::size_t scheme_dim>
  requires(scheme_dim == 2)
auto inline build_GkT(const Mesh_t &mesh, const auto &k_nodes,
                      const auto &c_nodes_mapping, const auto &xk, auto n0,
                      auto n1) {

  // edge with node n0, n1
  auto x0 = mesh.vertices(n0);
  auto x1 = mesh.vertices(n1);
  // compute over the triangle x0, x1, xk
  auto xT = x0 + x1 + xk;
  xT /= 3.0;
  // normal vector to 01k in direction of X, get also the surface of 01k
  // when the mesh is 2D, the normal is (0, 0), it has no meaning
  auto &&[v01k, surf_01k] = icmesh::triangle_VecNormalT(x0, x1, xk, xT);
  // tangential gradient
  icus::Tensor<double, mesh.dimension, mesh.dimension> AA;
  for (auto i = 0; i < mesh.dimension; ++i) {
    // different of x0 - xk ???
    AA(0, i) = xk[i] - x0[i];
    AA(1, i) = xk[i] - x1[i];
    if constexpr (mesh.dimension > 2)
      AA(2, i) = v01k[i];
  }
  auto BB = Inv_tensor<Mesh_t::dimension>(AA);
  typename Mesh_t::Vertex tmp0;
  typename Mesh_t::Vertex tmp1;
  for (auto i = 0; i < mesh.dimension; ++i) {
    tmp0[i] = BB(i, 0);
    tmp1[i] = BB(i, 1);
  }

  // todo: new object for each cell ???
  // todo: change the type ??? Already init to 0 because perso type of Vertex,
  // will not be the case with std::array
  std::vector<typename Mesh_t::Vertex> GkT(k_nodes.size());
  GkT[c_nodes_mapping(n0)] = tmp0;
  GkT[c_nodes_mapping(n1)] = tmp1;

  if constexpr (Mesh_t::dimension == 3)
    assert((check_GkT<Mesh_t, scheme_dim>(k_nodes, xk, v01k, GkT, mesh)));
  if constexpr (Mesh_t::dimension == 2)
    assert((check_GkT<Mesh_t, scheme_dim>(k_nodes, xk, GkT, mesh)));

  return std::tuple(GkT, surf_01k);
}

} // namespace numscheme
