#pragma once

#include <cassert>
#include <ctime>
#include <icus/Connectivity.h>
#include <scheme_lfv/scheme-lfv.h>
#include <scheme_lfv/vag-utils.h>
#include <set>
#include <variant>

namespace numscheme {
// allow array operations
using namespace compass::utils::array_operators;

using Labels = icus::Labels;

template <typename Mesh_t>
using VAG_Base =
    LFV_scheme<Mesh_t, Connectivity<ISet, ISet, 2>, Connectivity<ISet>>;

// vag scheme (no fracture)
// template <typename Mesh_t>
// todo: move the dimension template to the constructor, not the class, tmp
template <typename Mesh_t, std::size_t scheme_dim = Mesh_t::dimension>
  requires(scheme_dim > 0 && scheme_dim <= Mesh_t::dimension)
struct VAG : VAG_Base<Mesh_t> {
  // VAG scheme dimension : is mesh.dimension most of the time,
  // can be smaller (for example to simulate a fracture network without
  // the reservoir)
  static constexpr std::size_t dimension = scheme_dim;

  struct Builder {
    // contains the necessary info to build the scheme
    // we keep an instance in the scheme, which could be deleted
    // when it is not necessary anymore
    ISet _vag_nodes; // parent is mesh.nodes
    // connectivity this->cells -> mesh.nodes
    Connectivity<ISet> _cells_nodes;

    // mapping between mesh indices and site indices
    ISet _vag_cells_from_sites;
    // not possible to define it as an ISet because mesh nodes is not a
    // subset of sites.location (each mesh node is not necessarily a VAG node)
    Field<index_type> _mesh_nodes_sites_map;
  };

  // contains the necessary info to build the scheme, can be deleted after
  Builder build_info;

  // when VAG is applied on cells of dimension less than 3,
  // need to specify their thickness
  Scalar_field cells_thickness;

  // the VAG scheme can be called over a dimension smaller than the mesh one,
  // for example to simulate a fracture network (list of 2D objects : faces
  // in a 3D geometry)
  // dim contains the dimension of the numerical scheme,
  // for every dim, we call cells the highest dimension object of the num scheme
  // (faces in the previous example)
  VAG(Mesh_t &the_mesh, ISet vag_cells) : VAG_Base<Mesh_t>(the_mesh) {
    auto &&mesh = this->mesh;
    if (!vag_cells.is_included_in(mesh.elements[dimension])) {
      throw std::runtime_error(
          "The VAG cells are not included in the mesh cells!");
    }

    // todo : or this->cells = vag_cells ???
    if (vag_cells == mesh.elements[dimension]) {
      this->cells = vag_cells;
    } else {
      this->cells = vag_cells.rebase(mesh.elements[dimension]);
    }

    // if the VAG scheme is applied over objects of dimension < 3,
    // may need the cells thickness (init to 1 so there is no modif by default)
    if constexpr (dimension < 3) {
      cells_thickness = Scalar_field{this->cells};
      cells_thickness.fill(1.0);
    }

    // get connectivity between the cells (belong to mesh.elements[dimension])
    // and the nodes
    build_info._cells_nodes =
        mesh.template get_connectivity<dimension, mesh.node>().extract(
            this->cells, mesh.nodes);

    // get the nodes which connect to the vag cells
    build_info._vag_nodes = build_info._cells_nodes.image();

    // build the sites (union of vag cells and vag nodes)
    // there is no order conservation with join
    auto vag_sites = join(this->cells, build_info._vag_nodes);
    this->sites = Sites(vag_sites.rebase(mesh.joined_elements));
    // site id of the vag cells and all mesh nodes (isets of
    // build_info._cells_nodes)
    build_info._vag_cells_from_sites = this->cells.rebase(this->sites.location);
    auto vag_nodes_sites_map =
        build_info._vag_nodes.rebase(this->sites.location).mapping();
    // _vag_nodes parent is mesh.nodes
    build_info._mesh_nodes_sites_map = Field<index_type>(mesh.nodes);
    for (auto &&[mesh_i, site_i] : std::views::zip(
             build_info._vag_nodes.mapping(), vag_nodes_sites_map)) {
      build_info._mesh_nodes_sites_map(mesh_i) = site_i;
    }

    // fill flux_tips and stencil (in sites indices)
    std::vector<Pair> tips;
    std::vector<std::vector<index_type>> vag_stencil;
    tips.reserve(this->cells.size() * 8);        // 8 is arbitrary
    vag_stencil.reserve(this->cells.size() * 8); // 8 is arbitrary
    for (auto &&[c, neigh_n] : build_info._cells_nodes.targets_by_source()) {
      // c in this->cells, neigh_n in mesh.nodes
      std::vector<index_type> cell_stencil;
      cell_stencil.push_back(build_info._vag_cells_from_sites.mapping()[c]);
      for (auto &&n : neigh_n)
        cell_stencil.push_back(build_info._mesh_nodes_sites_map(n));

      for (auto &&n : neigh_n) {
        tips.push_back({build_info._vag_cells_from_sites.mapping()[c],
                        build_info._mesh_nodes_sites_map(n)});
        vag_stencil.push_back(cell_stencil);
      }
    }
    tips.shrink_to_fit();
    vag_stencil.shrink_to_fit();

    auto fluxes = ISet{tips.size()};
    // build connectivity with source = fluxes iset; target = sites iset,
    // preserve_targets_order = true
    this->flux_tips = {fluxes, this->sites, tips, true};
    this->stencils = {fluxes, this->sites, vag_stencil, true};
  }

  template <typename lambda_t> lambda_t build_lambda(lambda_t lambda) {
    // in VAG lambda is accessed via the cell index
    return lambda.extract(this->cells);
  }

  // distribute cell volumes over sites
  template <typename Quantity_t>
  // is_volume_nodes is carried by mesh.nodes (can be different from scheme
  // nodes)
  Quantity_t distribute_volumes(Quantity_t quantities, Labels sites_labels,
                                Field<bool> is_volume_nodes, double omega) {
    auto sites_quantities = Quantity_t(this->sites);
    sites_quantities.fill(0.0);
    auto cells_quantities = quantities.extract(this->cells);
    for (auto &&[c, neigh_n] : build_info._cells_nodes.targets_by_source()) {
      // c in this->cells, neigh_n in mesh.nodes
      auto NbVolume = 0;
      auto NbInternalVolume = 0;
      auto c_quantities = cells_quantities(c);
      // _vag_cells_from_sites parent is sites.location
      auto c_site_i = build_info._vag_cells_from_sites.mapping()[c];
      // count the nodes of cell c which have a volume, and those with the same
      // label as the cell label
      for (auto &&n : neigh_n) {
        if (is_volume_nodes(n)) {
          ++NbVolume;
          if (sites_labels(c_site_i) ==
              sites_labels(build_info._mesh_nodes_sites_map(n)))
            ++NbInternalVolume;
        }
      }
      // use "*=" to allow c_quantities to be an array
      auto split_quantities = c_quantities;
      split_quantities *= omega * NbVolume / NbInternalVolume;
      // affect volume to the nodes and compute the remaining cell volume
      for (auto &&n : neigh_n) {
        if (is_volume_nodes(n) &&
            sites_labels(c_site_i) ==
                sites_labels(build_info._mesh_nodes_sites_map(n))) {
          c_quantities -= split_quantities;
          sites_quantities(build_info._mesh_nodes_sites_map(n)) +=
              split_quantities;
        }
      }
      sites_quantities(c_site_i) = c_quantities;
    }
    return sites_quantities;
  }

  // distribute surface quantities over sites
  // used to distribute the Neumann flux written on faces over
  // the neighbouring sites
  // what about 2D ??????
  // does not work if dimension = 1 (1D num scheme)
  template <typename Quantity_t>
    requires(dimension == 3)
  Field<Quantity_t> distribute_surfaces(Field<Quantity_t> quantities) {
    using Vertex = Mesh_t::Vertex;
    auto sites_quantities = Field<Quantity_t>(this->sites);
    sites_quantities.fill(Quantity_t{0.0});

    // if there is nothing to distribute, return field with 0 values
    if (quantities.size() == 0)
      return sites_quantities;

    auto &&mesh = this->mesh;
    // the support of the quantities must be a set of the mesh with codimension
    // 1
    assert(quantities.support.is_included_in(mesh.elements[dimension - 1]));

    // connectivity quantities.support -> mesh.nodes
    auto faces_nodes =
        mesh.template get_connectivity<mesh.face, mesh.node>().extract(
            quantities.support, mesh.nodes);
    // connectivity quantities.support -> mesh.edges
    auto faces_edges =
        mesh.template get_connectivity<mesh.face, mesh.edge>().extract(
            quantities.support, mesh.edges);
    auto edges_nodes = mesh.template get_connectivity<mesh.edge, mesh.node>();

    // get support measures and centers
    auto face_measures = mesh.get_face_measures().extract(quantities.support);
    // face_centers is a vertex field
    auto face_centers =
        mesh.get_face_isobarycenters().extract(quantities.support);

    // alpha (Scalar_field) contains the surface allocated to each node
    auto alpha = Scalar_field(mesh.nodes);

    // loop over face f in quantities.support and its nodes
    for (auto &&[f, f_nodes] : faces_nodes.targets_by_source()) {
      // fill alpha for each node s
      // = (f_area / nb_nodes + area of ((s-1) - s - (s+1) - f_center)) / 3
      // ((s-1) - s - (s+1) - f_center)) is decomposed into 2 triangles
      auto f_center = face_centers(f);
      auto f_measure_by_node = face_measures(f) / f_nodes.size() / 3.0;

      // init alpha with face_surface/nb_nodes/3.0
      for (auto &&s : f_nodes)
        alpha(s) = f_measure_by_node;

      // loop over the edges of f
      for (auto &&e : faces_edges.targets_by_source(f)) {
        // get its nodes to compute the triangle area (edge + face center)
        auto &&[s1, s2] = edges_nodes.targets_by_source(e);
        auto third_area = icmesh::triangle_area<Mesh_t::dimension>(
                              mesh.vertices(s1), mesh.vertices(s2), f_center) /
                          3.0;
        // add contribution to both nodes
        alpha(s1) += third_area;
        alpha(s2) += third_area;
      }

      // fill sites_quantities
      for (auto &&s : f_nodes)
        sites_quantities(build_info._mesh_nodes_sites_map(s)) +=
            quantities(f) * alpha(s);
    }

    return sites_quantities;
  }

  template <typename Quantity_t>
    requires(dimension < 3)
  Field<Quantity_t> distribute_surfaces(Field<Quantity_t> quantities) {
    using Vertex = Mesh_t::Vertex;
    auto sites_quantities = Field<Quantity_t>(this->sites);
    sites_quantities.fill(Quantity_t{0.0});

    // if there is nothing to distribute, return field with 0 values
    if (quantities.size() == 0)
      return sites_quantities;

    throw std::runtime_error(
        "VAG distribute_surfaces not implemented for VAG dim < 3");

    return sites_quantities;
  }

  bool check_CV(Field<bool> is_vol_nodes, Scalar_field sites_CV,
                double eps = 1e-6) {

    bool everything_ok = true;

    // check cells
    for (auto &&c : this->cells) {
      // _vag_cells_from_sites parent is sites.location
      auto c_site_i = build_info._vag_cells_from_sites.mapping()[c];
      if (sites_CV(c_site_i) < eps) {
        std::cout << "cell controle volume < " << eps;
        std::cout << ", cell index (in vag cell iset) " << c;
        std::cout << ", volume " << sites_CV(c_site_i) << std::endl;
        everything_ok = false;
      }
    }

    // check nodes
    // loop over vag nodes using the mesh.nodes index
    for (auto &&n : build_info._vag_nodes.mapping()) {
      if (is_vol_nodes(n) &&
          sites_CV(build_info._mesh_nodes_sites_map(n)) < eps) {
        std::cout << "non Dirichlet node controle volume < " << eps;
        std::cout << ", node index (in mesh.nodes iset) " << n;
        std::cout << ", volume "
                  << sites_CV(build_info._mesh_nodes_sites_map(n)) << std::endl;
        everything_ok = false;
      }
    }

    // sum of the CV = volume of the vag cells
    double cumul_CV = 0.0;
    for (auto &&s : this->sites)
      cumul_CV += sites_CV(s);
    // diff with the VAG cells
    // sometimes is faces volumes !
    auto vag_cells_measure =
        this->mesh.template get_measures<dimension>().extract(this->cells);
    double cumul_cells_vol = 0.0;
    for (auto &&c : this->cells)
      cumul_cells_vol += vag_cells_measure(c);
    if (fabs(cumul_cells_vol - cumul_CV) / cumul_cells_vol > 1e-12) {
      std::cout << "Total of VAG controle volumes: " << cumul_CV;
      std::cout << ", is distinct from VAG cells volume: " << cumul_cells_vol
                << std::endl;
      std::cout << "Relative difference is: "
                << fabs(cumul_cells_vol - cumul_CV) / cumul_cells_vol
                << std::endl;
      everything_ok = false;
    }

    return everything_ok;
  }

  // build the transmissivity factors
  // Tk(s, s') (uk - us') => Tks(k) += Tk(s, s') and Tks(s') -= Tk(s, s')
  template <typename lambda_t>
    requires(dimension == 3)
  Scalar_field build_transmissivities(lambda_t lambda) {
    auto scheme_lambda = build_lambda(lambda);
    auto trans = Scalar_field(this->stencils.connections());
    trans.fill(0.0);
    auto &&mesh = this->mesh;
    // get cell centers
    auto vag_cell_centers =
        mesh.template get_isobarycenters<dimension>().extract(this->cells);
    // get face centers (face keep their global indices)
    auto face_centers = mesh.get_face_isobarycenters();
    // connectivity this->cells -> mesh.faces
    auto cells_faces =
        mesh.template get_connectivity<mesh.cell, mesh.face>().extract(
            this->cells, mesh.faces);
    // connectivity mesh.faces -> mesh.nodes
    auto faces_nodes = mesh.template get_connectivity<mesh.face, mesh.node>();
    // connectivity mesh.faces -> mesh.edges
    auto faces_edges = mesh.template get_connectivity<mesh.face, mesh.edge>();
    // connectivity mesh.edges -> mesh.nodes
    auto edges_nodes = mesh.template get_connectivity<mesh.edge, mesh.node>();

    // to save the index of the nodes in the face node list
    auto &&f_nodes_mapping = Field<index_type>(mesh.nodes);

    // to find the flux index of each tip (k, s),
    // store the first index concerning the cell k
    index_type k_first_flux_i = 0;

    for (auto &&[k, k_faces] : cells_faces.targets_by_source()) {
      auto xk = vag_cell_centers(k);
      auto k_nodes = build_info._cells_nodes.targets_by_source(k);
      auto nb_k_nodes = k_nodes.size();
      // loop over the faces of the cell k
      for (auto &&f : k_faces) {
        // nodes in face f
        auto f_nodes = faces_nodes.targets_by_source(f);
        auto nb_f_nodes = f_nodes.size();
        // init the mapping between the index of the node and
        // its position in f_nodes
        for (auto &&[in, n] : std::views::enumerate(f_nodes))
          f_nodes_mapping(n) = in;
        // to fill Tks(k) and Tks(s'), compute the mapping between the position
        // of the node in f_nodes and the indices in the stencil field
        // for Tks(k) in stencil_conn_n0k_i and Tks(s') in stencil_conn_n01_i
        auto &&[stencil_conn_n0k_i, stencil_conn_n01_i] =
            f_nodes2stencil_n0k(k, f_nodes, k_nodes, k_first_flux_i);

        // loop over the edges of face f
        for (auto &&e : faces_edges.targets_by_source(f)) {
          // edge with node n0, n1
          auto &&[n0, n1] = edges_nodes.targets_by_source(e);

          auto &&[GkT, volT] = build_GkT<Mesh_t, dimension>(
              mesh, f_nodes, f_nodes_mapping, xk, face_centers(f), n0, n1);

          // fill the transmissivity factors
          // Tk(s, s') (uk - us') => Tks(k) += Tk(s, s')
          // and Tks(s') -= Tk(s, s')
          // loop over the node s
          for (auto fn0_i = 0; fn0_i < nb_f_nodes; ++fn0_i) {
            // loop over the node s'
            for (auto fn1_i = 0; fn1_i < nb_f_nodes; ++fn1_i) {
              auto T01 = volT * compute_T01<Mesh_t::dimension>(
                                    scheme_lambda(k), GkT[fn0_i], GkT[fn1_i]);
              trans(stencil_conn_n0k_i[fn0_i]) += T01;
              trans(stencil_conn_n01_i[fn0_i][fn1_i]) -= T01;
            } // n1
          } // n0

        } // end edge of f
      } // end face f of k

      // next cell in stencil, jump n_tips
      k_first_flux_i += nb_k_nodes;

    } // end cell k

    assert(check_trans(trans, this->stencils));

    return trans;
  }

  // when 2D scheme in a 3D mesh, the computation of the transmissivities
  // is correct only with **isotropic lambda**, so scalar_field
  // with 2D scheme in a 2D mesh, the diffusion can be anisotropic
  template <typename lambda_t>
    requires(dimension == 2 &&
             (Mesh_t::dimension == 2 ||
              Mesh_t::dimension == 3 && std::is_same_v<lambda_t, Scalar_field>))
  Scalar_field build_transmissivities(lambda_t lambda) {

    auto scheme_lambda = build_lambda(lambda);
    auto trans = Scalar_field(this->stencils.connections());
    trans.fill(0.0);
    auto &&mesh = this->mesh;
    // here "cells" are 2D objects in a 3D mesh (ie faces)
    // OR 2D objects in a 2D mesh,
    // get "cell" centers
    auto vag_cell_centers = mesh.isobarycenters(this->cells);
    // faces = edges are 1D objects
    // connectivity this->cells -> mesh.elements[dimension-1] = mesh.edges
    auto cells_edges =
        mesh.template get_connectivity<dimension, mesh.edge>().extract(
            this->cells, mesh.edges);
    // connectivity mesh.edges -> mesh.nodes
    auto edges_nodes = mesh.template get_connectivity<mesh.edge, mesh.node>();

    // to save the index of the nodes in the cell node list
    auto &&c_nodes_mapping = Field<index_type>(mesh.nodes);

    // to find the flux index of each tip (k, s),
    // store the first index concerning the cell k
    index_type k_first_flux_i = 0;

    // loop over the cells k, get also the edges connected to k
    for (auto &&[k, k_edges] : cells_edges.targets_by_source()) {
      auto k_nodes = build_info._cells_nodes.targets_by_source(k);
      auto nb_k_nodes = k_nodes.size();
      // ok whatever the cell center if it is star shaped (not necessarily
      // the barycenter)
      auto xk = vag_cell_centers(k);

      // init the mapping between the index of the node and
      // its position in k_nodes
      for (auto &&[in, n] : std::views::enumerate(k_nodes))
        c_nodes_mapping(n) = in;
      // to fill Tks(k) and Tks(s'), compute the mapping between the position
      // of the node in k_nodes and the indices in the stencil field
      // for Tks(k) in stencil_conn_n0k_i and Tks(s') in stencil_conn_n01_i
      auto &&[stencil_conn_n0k_i, stencil_conn_n01_i] =
          k_nodes2stencil_n0k(k, k_nodes, k_first_flux_i);

      // loop over the edges of cell k (edge is of codimension 1 compared to
      // cells)
      for (auto &&e : k_edges) {
        // edge with node n0, n1
        auto &&[n0, n1] = edges_nodes.targets_by_source(e);

        auto &&[GkT, surf_01k] = build_GkT<Mesh_t, dimension>(
            mesh, k_nodes, c_nodes_mapping, xk, n0, n1);

        auto in0 = c_nodes_mapping(n0);
        auto in1 = c_nodes_mapping(n1);

        // fill the transmissivity factors
        // Tk(s, s') (uk - us') => Tks(k) += Tk(s, s')
        // and Tks(s') -= Tk(s, s')
        // Rq: GkT(:,j)=0 if j is not in0, in1
        // loop over the node s
        for (auto cn0_i : {in0, in1}) {
          // loop over the node s'
          for (auto cn1_i : {in0, in1}) {
            auto T01 = cells_thickness(k) * surf_01k *
                       compute_T01<Mesh_t::dimension>(scheme_lambda(k),
                                                      GkT[cn0_i], GkT[cn1_i]);
            trans(stencil_conn_n0k_i[cn0_i]) += T01;
            trans(stencil_conn_n01_i[cn0_i][cn1_i]) -= T01;
          } // n1
        } // n0

        // reset the modified coeff
        GkT[c_nodes_mapping(n0)].fill(0.0);
        GkT[c_nodes_mapping(n1)].fill(0.0);
      } // end edge e

      // next cell in stencil, jump n_tips
      k_first_flux_i += nb_k_nodes;

    } // end cell k

    assert(check_trans(trans, this->stencils));

    return trans;
  }

  template <typename lambda_t>
    requires(dimension < 2)
  void build_transmissivities(lambda_t lambda) {

    throw std::runtime_error(
        "VAG build_transmissivities not implemented with dimension < 2");
  }

  bool inline check_trans(Scalar_field trans, Connectivity<ISet> t_stencils) {
    for (auto &&flux_i : this->stencils.source()) {
      auto t_sum = 0.0;
      for (auto &&tf_i : this->stencils.connections_by_source(flux_i))
        t_sum += trans(tf_i);
      if (t_sum > 1e-12)
        return false;
    }
    return true;
  }

  // print transmissivities together with flux tips and stencil
  void print_trans(Scalar_field trans) {
    compass::utils::Pretty_printer print;
    print.endl = "\n";
    for (auto &&[flux_i, tips_i] :
         std::views::enumerate(this->flux_tips.targets())) {
      auto &&[s0, s1] = tips_i;

      print("-------------------");
      print("flux", flux_i, "has tips (in site indices)", s0, s1);

      auto &&cf = this->stencils.connections_by_source(flux_i);
      auto &&tf = this->stencils.targets_by_source(flux_i);
      for (auto &&[cf_i, tf_i] : std::views::zip(cf, tf))
        print("trans for site", tf_i, "is", trans(cf_i));
    }
  }

  template <std::size_t dim>
  static inline Scalar compute_T01(Scalar lambda_k, const auto &GkT0,
                                   const auto &GkT1) {
    // scalar product between the diagonals of GkT0 and GkT1
    Scalar T01 = 0.0;
    for (auto &&[GkT0_i, GkT1_i] : std::views::zip(GkT0, GkT1))
      T01 += GkT0_i * GkT1_i;
    return lambda_k * T01;
  }

  template <std::size_t dim, typename Tensor = icus::Tensor<double, dim, dim>>
  static inline Scalar compute_T01(Tensor &lambda_k,
                                   const auto &GkT0, // const tensor !
                                   const auto &GkT1) {
    Scalar T01 = 0.0;
    for (auto i = 0; i < dim; ++i) {
      for (auto j = 0; j < dim; ++j)
        T01 += lambda_k(i, j) * GkT0[i] * GkT1[j];
    }
    return T01;
  }

  // in some tabular, the node index is the position in faces_nodes(f),
  // in other tabular, the node index is the position in cells_nodes(k)
  inline auto f_nodes2stencil_n0k(auto k, const auto &f_nodes,
                                  const auto &c_nodes, auto k_first_flux_i) {
    // first compute the mapping between the node index in f_nodes
    // and the node index in c_nodes
    auto nb_f_nodes = f_nodes.size();
    std::vector<index_type> f_nodes2c_nodes_mapping(nb_f_nodes);
    for (auto &&[fn_i, fn] : std::views::enumerate(f_nodes)) {
      auto it = std::find(c_nodes.begin(), c_nodes.end(), fn);
      assert(it != c_nodes.end());
      index_type cn_i = std::distance(c_nodes.begin(), it);
      f_nodes2c_nodes_mapping[fn_i] = cn_i;
    }

    // flux indices for each s, ie the connection index of the tip (k, s)
    // compute the indices concerning Tks(k) for each s
    std::vector<index_type> stencil_conn_n0k_index(nb_f_nodes);
    // compute the indices concerning Tks(s') for each s, each s'
    std::vector<std::vector<index_type>> stencil_conn_n01_index(
        nb_f_nodes, std::vector<index_type>(nb_f_nodes));
    for (auto fn0_i = 0; fn0_i < nb_f_nodes; ++fn0_i) {
      auto cn0_i = f_nodes2c_nodes_mapping[fn0_i];
      // tips are k, cn0
      // look for the corresponding flux index to get the stencil, flux_tips...
      auto flux_i = k_first_flux_i + cn0_i;
      // _vag_cells_from_sites parent is sites.location
      auto k_site_i = build_info._vag_cells_from_sites.mapping()[k];
      // assert the flux concerns K, the tip is k->s
      assert(this->flux_tips.targets_by_source(flux_i)[0] == k_site_i);

      auto &&cf = this->stencils.connections_by_source(flux_i);
      auto &&tf = this->stencils.targets_by_source(flux_i);
      // get the index of the transmissiviy field of the cell k
      // ie look for connection index of the cell k in the stencil
      // to fill Tks(k)
      auto it_k = std::find(tf.begin(), tf.end(), k_site_i);
      assert(it_k != tf.end());
      auto k_index = std::distance(tf.begin(), it_k);
      stencil_conn_n0k_index[fn0_i] = cf[k_index];

      // loop over the node s'
      for (auto &&[fn1_i, fn1] : std::views::enumerate(f_nodes)) {
        // get the index of the transmissiviy field of the node s'
        // ie look for connection index of the node s' in the stencil
        // to fill Tks(s')
        auto it_n1 = std::find(tf.begin(), tf.end(),
                               build_info._mesh_nodes_sites_map(fn1));
        assert(it_n1 != tf.end());
        auto n1_index = std::distance(tf.begin(), it_n1);
        stencil_conn_n01_index[fn0_i][fn1_i] = cf[n1_index];
      } // n1

    } // n0
    return std::make_tuple(stencil_conn_n0k_index, stencil_conn_n01_index);
  }

  // from the list of cell nodes, get the position in the stencil list
  inline auto k_nodes2stencil_n0k(auto k, const auto &k_nodes,
                                  auto k_first_flux_i) {
    auto nb_k_nodes = k_nodes.size();
    // compute the indices concerning Tks(k) for each s
    std::vector<index_type> stencil_conn_n0k_index(nb_k_nodes);
    // compute the indices concerning Tks(s') for each s, each s'
    std::vector<std::vector<index_type>> stencil_conn_n01_index(
        nb_k_nodes, std::vector<index_type>(nb_k_nodes));
    for (auto cn0_i = 0; cn0_i < nb_k_nodes; ++cn0_i) {
      // tips are k, cn0
      // look for the corresponding flux index to get the stencil, flux_tips...
      auto flux_i = k_first_flux_i + cn0_i;
      // _vag_cells_from_sites parent is sites.location
      auto k_site_i = build_info._vag_cells_from_sites.mapping()[k];
      // assert the flux concerns K
      assert(this->flux_tips.targets_by_source(flux_i)[0] == k_site_i);

      auto &&cf = this->stencils.connections_by_source(flux_i);
      auto &&tf = this->stencils.targets_by_source(flux_i);
      // get the index of the transmissiviy field of the cell k
      // ie look for connection index of the cell k in the stencil
      // to fill Tks(k)
      auto it_k = std::find(tf.begin(), tf.end(), k_site_i);
      assert(it_k != tf.end());
      auto k_index = std::distance(tf.begin(), it_k);
      stencil_conn_n0k_index[cn0_i] = cf[k_index];

      // loop over the node s'
      for (auto &&[cn1_i, cn1] : std::views::enumerate(k_nodes)) {
        // get the index of the transmissiviy field of the node s'
        // ie look for connection index of the node s' in the stencil
        // to fill Tks(s')
        auto it_n1 = std::find(tf.begin(), tf.end(),
                               build_info._mesh_nodes_sites_map(cn1));
        assert(it_n1 != tf.end());
        auto n1_index = std::distance(tf.begin(), it_n1);
        stencil_conn_n01_index[cn0_i][cn1_i] = cf[n1_index];
      } // n1

    } // n0
    return std::make_tuple(stencil_conn_n0k_index, stencil_conn_n01_index);
  }

  // give the same label for cells which have same lambda values
  // same lambda = same max coeff if anisotropic
  // lambda is carried by the cells
  template <typename lambda_t> Labels build_cells_labels(lambda_t lambda) {
    assert(lambda.support == this->cells);
    double eps = 1.e-7;
    compass::utils::Pretty_printer print;
    auto cells_labels = Labels(this->cells);
    // store the different max values of lambda to color the cells
    std::vector<double> diff_val;
    for (auto &&c : this->cells) {
      auto c_max_d = cell_perm_max(lambda(c));
      auto it = std::find_if(diff_val.begin(), diff_val.end(),
                             [c_max_d, eps](double d) {
                               return fabs((d - c_max_d) / c_max_d) < eps;
                             });
      if (it == diff_val.end()) {
        cells_labels(c) = diff_val.size();
        diff_val.push_back(c_max_d);
      } else {
        auto diff_i = std::distance(diff_val.begin(), it);
        cells_labels(c) = diff_i;
      }
    }

    return cells_labels;
  }

  template <typename lambda_t>
  Labels build_sites_labels(lambda_t lambda, const Labels &cells_labels) {
    assert(lambda.support == cells_labels.support);
    assert(cells_labels.support == this->cells);
    auto sites_labels = Labels(this->sites);
    // store cell label into sites_labels
    for (auto &&[K, label] : cells_labels.enumerate())
      sites_labels(build_info._vag_cells_from_sites.mapping()[K]) = label;

    // get the connectivity of the vag nodes and the vag cells
    // build_info._cells_nodes contains the connectiviy of the vag cells
    // to the mesh nodes
    auto vag_nodes_cells =
        build_info._cells_nodes.restrict_target(build_info._vag_nodes)
            .transpose();
    for (auto &&[vag_n, cells] : vag_nodes_cells.targets_by_source()) {
      // get the index of the node in the mesh nodes iset
      // _vag_nodes parent is mesh nodes
      auto n = build_info._vag_nodes.mapping()[vag_n];

      // node label is the label of the cell with the highest permeability
      // find this cell
      auto perm_max = 0.0;
      auto K_max = -1;
      for (auto &&K : cells) {
        // find the max value of the perm (scalar or tensor)
        auto c_perm_max = cell_perm_max(lambda(K));
        if (c_perm_max > perm_max) {
          perm_max = c_perm_max;
          K_max = K;
        }
      }
      assert(K_max >= 0);
      // find sites index of node n, store label index
      sites_labels(build_info._mesh_nodes_sites_map(n)) = cells_labels(K_max);
    }

    return sites_labels;
  }

  static inline Scalar cell_perm_max(Scalar cell_perm) { return cell_perm; }

  template <typename K_t> static inline Scalar cell_perm_max(K_t cell_perm) {
    return *std::max_element(std::begin(cell_perm), std::end(cell_perm));
  }
};

} // namespace numscheme
