#pragma once

#include <ranges>

#include <icmesh/Geom_utils.h>
#include <icmesh/Geomtraits.h>
#include <icus/Connectivity.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <icus/types.h>
#include <pscal/multistate-operators.h>
#include <pscal/value.h>

namespace numscheme {
using namespace icus;

using Stencil = std::vector<index_type>;
using Pair = std::array<index_type, 2>; // std::pair<index_type>;
using Scalar = double;
using Scalar_field = Field<Scalar>;
template <size_t dim, typename Scalar = double>
using Tensor_d = icus::Tensor<Scalar, dim, dim>;
template <size_t dim, typename Scalar = double>
using Tensor_field = Field<Tensor_d<dim, Scalar>>;
template <typename mesh_t> using _Mesh_t = icmesh::Geomtraits<mesh_t>;

struct Sites : ISet { // a site = ISet + location
  // Sites can be seen as an ISet with location as parent,
  // but we want to keep the location info when "rebasing"
  // this iset on the union of sites
  // thus we create a new iset Sites and we will set a base later,
  // keeping location
  // 2023-01-26: SL: peut etre devrait on garder physics aussi comme cela on
  // aurait les trois étages ?
  ISet location; // when there is a meaning, parent is mesh.joined_elements
  // which is an iset containing all mesh objects :
  // (cells - faces - edges - nodes)
  // sometimes there is no mesh relation to the sites ???
  Sites() = default;
  Sites(const ISet &I) : ISet{I.size()}, location{I} {}
  bool is_consistent() const { return size() == location.size(); }
  // get the equivalent iset in the sites tree
  ISet from_location(ISet is) const {
    auto mapping = is.resolve_mapping(location);
    return (*this).extract(mapping);
  }
  // get the equivalent iset in the location tree
  ISet to_location(ISet is) const {
    auto mapping = is.resolve_mapping(*this);
    return location.extract(mapping);
  }
};

// LinearFiniteVolume_scheme
template <typename Mesh_type, typename Flux_conn, typename Sten_conn>
struct LFV_scheme {
  using Mesh_t = Mesh_type;
  // todo move mesh and cells into Builder ???
  Mesh_t mesh; // contains the mesh together with geometric utilities
  ISet cells;  // cells where the num scheme is applied, is mesh.cells or parent
               // is mesh.cells, can be faces if fracture network in a 3D
               // geometry, it depends on the dim of the numerical scheme
  Sites sites;
  // in the following comments "fluxes" is an iset
  // counting the flux arrows
  // tips of the flux arrow
  Flux_conn flux_tips; // CRS storage (fluxes, sites)
  // for each flux, contains the sites necessary for the approximation
  Sten_conn stencils;

  LFV_scheme(Mesh_t &the_mesh) : mesh{the_mesh} {}
};

// // coupling_LinearFiniteVolume_scheme
// // coupling between two already existing LFV_scheme
// struct coupling_LFV_scheme : LFV_scheme {
//   ISet sites_phy0; // one physics (Coats, Wells...)
//   ISet sites_phy1; // one physics (maybe different from phy0)
//   // ...
// };

template <typename State_t> struct flux_vector {
  State_t::value_t value;
  // size will be local stencil: 2 in tpfa, nb_nodes + 1 in vag
  std::vector<State_t> derivatives;
};

// todo : remove the scheme from LFVS_flux
template <typename Scheme, typename State_variable>
// LinearFiniteVolumeScheme_flux
// K grad(Y) where Y is phase pressure or temperature
struct LFVS_flux {
  Scheme scheme; // use LFV_scheme (and no TPFA) in binding : copy is light
  Scalar_field trans;
  State_variable var; // P^\alpha or T

  template <typename State_t>
  // states is a field carried by sites
  auto operator()(index_type flux, const Field<State_t> &states) const {
    auto &&stencils = scheme.stencils;
    flux_vector<State_t> res;
    res.value = 0.0;
    // trans is a field carried by the Connectivity stencils
    // ie by the connections iset related to the connectivity
    // flux is a source index of stencils
    // stencils.connections_by_source(flux) gives all the connections indices
    // stencils.targets_by_source(flux) gives all the targets indices
    // related to the source "flux"
    auto &&cf = stencils.connections_by_source(flux);
    auto &&tf = stencils.targets_by_source(flux);
    assert(cf.size() == tf.size());
    for (auto &&[cf_i, tf_i] : std::views::zip(cf, tf)) {
      auto &&var_state = trans(cf_i) * var(states(tf_i)); // var is P_alpha or T
      res.value += var_state.value;
      res.derivatives.push_back(var_state.derivatives);
    }
    return res;
  }
};

// todo : remove the scheme from LFVS_gravity
// LinearFiniteVolumeScheme_gravity
// rho * (gravity scalar position)
template <typename Scheme, typename Density> struct LFVS_gravity {
  Scheme scheme; // use LFV_scheme (and no TPFA) in binding : copy is light
  Scalar_field trans;
  Density connection_density; // phase volumetric mass dens of the connection
  Scheme::Mesh_t::Vertex gravity;
  Scheme::Mesh_t::Vertex_field position;

  LFVS_gravity(const Scheme &num_scheme, Scalar_field T, const Density &rho,
               Scheme::Mesh_t::Vertex g)
      : scheme{num_scheme}, trans{T}, connection_density{rho}, gravity{g} {
    // get positions of all sites
    position = scheme.mesh.isobarycenters(scheme.sites.location);
  }

  template <typename State_t, typename Context_t>
  // states is a field carried by sites
  auto operator()(index_type flux, const Field<State_t> &states,
                  const Field<Context_t> &contexts) const {
    // get both extremities of the flux
    auto &&[s0, s1] = scheme.flux_tips.targets_by_source(flux);
    // compute a rho value (and derivatives wrt s0, s1)
    // depending on the present phases
    // at both extremities of the flux
    auto &&rho = connection_density.template average<flux_vector<State_t>>(
        s0, s1, states, contexts);

    // compute value and derivatives of the gravity term depending on the
    // stencil
    flux_vector<State_t> res;
    res.value = 0.0;

    auto &&stencils = scheme.stencils;
    // trans is a field carried by the Connectivity stencils
    // ie by the connections iset related to the connectivity
    // flux is a source index of stencils
    // stencils.connections_by_source(flux) gives all the connections indices
    // stencils.targets_by_source(flux) gives all the targets indices
    // related to the source "flux"
    auto &&cf = stencils.connections_by_source(flux);
    auto &&tf = stencils.targets_by_source(flux);
    assert(cf.size() == tf.size());

    // s0_found and s1_found just for debug in assert
    auto s0_found = false;
    auto s1_found = false;
    for (auto &&[cf_i, tf_i] : std::views::zip(cf, tf)) {
      // Sum over stencil (Ti * rho_average * (g scalar position))
      auto pos = position(tf_i);
      // todo: we could maybe compute pos scalar gravity as a preprocessing
      // but in this case we must update the scalar values when modifying g
      auto Ti_g = trans(cf_i) * std::inner_product(pos.begin(), pos.end(),
                                                   gravity.begin(), 0.0);
      res.value += Ti_g * rho.value;
      // fill derivatives of Ti * rho * (g scalar pos) (derivatives of rho
      // depends only on s0 and s1) trans * (g scalar pos) is a constant
      if (tf_i == s0) {
        assert(!s0_found);
        s0_found = true;
        // pscal object allows to multiply by a cst
        auto pscal_rho0 = pscal::Scalar<State_t>{rho.value, rho.derivatives[0]};
        auto Ti_g_rho0 = Ti_g * pscal_rho0;
        res.derivatives.push_back(Ti_g_rho0.derivatives);
      } else if (tf_i == s1) {
        assert(!s1_found);
        s1_found = true;
        // pscal object allows to multiply by a cst
        auto pscal_rho1 = pscal::Scalar<State_t>{rho.value, rho.derivatives[1]};
        auto Ti_g_rho1 = Ti_g * pscal_rho1;
        res.derivatives.push_back(Ti_g_rho1.derivatives);
      } else {
        // 0.0
        res.derivatives.push_back(State_t{});
      }
    }
    assert(s0_found && s1_found);

    return res;
  }
};

} // namespace numscheme
