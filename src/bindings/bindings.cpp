#include <icus/ISet.h>
#include <nanobind/nanobind.h>
#include <scheme_lfv/scheme-lfv.h>

namespace nb = nanobind;

void bind_sites(nb::module_ &module) {
  auto cls = nb::class_<numscheme::Sites, icus::ISet>(
      module, "Sites", "ISet with location attribute");
  cls.def(nb::init<>());
  cls.def(nb::init<const numscheme::Sites &>());
  cls.def(nb::init<const icus::ISet &>());
  cls.def_ro("location", &numscheme::Sites::location);
  cls.def("is_consistent", &numscheme::Sites::is_consistent);
  cls.def("from_location", &numscheme::Sites::from_location);
  cls.def("to_location", &numscheme::Sites::to_location);
}

NB_MODULE(bindings, module) {

  // we import icus to be able to handle icus objects on the python side
  nb::module_::import_("icus");

  bind_sites(module);
}
